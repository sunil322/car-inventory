function get_car_info_older_2000(inventory){

    if(Array.isArray(inventory)){
        const car_list=[];
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].car_year<=2000){
            car_list.push(inventory[index]);
            }
        }
        return car_list;
    }else{
        return [];
    }
}

 module.exports = get_car_info_older_2000