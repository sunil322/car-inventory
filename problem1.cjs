function get_car_info_by_id(inventory,id){

    if(Array.isArray(inventory)){

        let car_info_found=false;
        if(isNaN(id)){
            return [];
        }
        for(let index=0;index<inventory.length;index++){
        if(inventory[index].id==id){
            car_info_found=true;
            return inventory[index];
        }}
        if(!car_info_found){
        return [];
        }
    }
    else{
        return [];
    }

}

module.exports=get_car_info_by_id;