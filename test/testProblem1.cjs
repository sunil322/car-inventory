const get_car_info_by_id = require('../problem1.cjs');
const inventory = require('../inventory.cjs');

const result = get_car_info_by_id([{"id":0,"car_make":"dodge","car_model":"Nag","car_year":2011}],0);

if(Array.isArray(result)){
    console.log(result);
}
else{
    console.log("Car "+result.id+" is a "+result.car_year+" "+result.car_make+" "+result.car_model);
}