const inventory = require('../inventory.cjs');
const get_last_car_info = require('../problem2.cjs');

const car_info = get_last_car_info(inventory);

if(Array.isArray){
    console.log(car_info)
}else{
    console.log("Last car is a "+car_info.car_make+" "+car_info.car_model);
}