function get_BMW_Audi_car_list(inventory){
    if(Array.isArray(inventory)){
        const car_list=[];
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].car_make == 'Audi' || inventory[index].car_make == 'BMW'){
            car_list.push(inventory[index]);
            }
        }
    return car_list;
    }else{
        return [];
    }
}

module.exports = get_BMW_Audi_car_list;