function sort_car_model(inventory){
    if(Array.isArray(inventory)){
        let model_list = new Set();
        for(let index=0;index<inventory.length;index++){
            model_list.add(inventory[index].car_model);
        }   
        return Array.from(model_list).sort((model1,model2)=>{
        return model1.toLowerCase().localeCompare(model2.toLowerCase());
        });
    }else{
        return [];
    }
}

module.exports = sort_car_model;