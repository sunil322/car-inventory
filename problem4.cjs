function get_car_year_list(inventory){
   if(Array.isArray(inventory)){
    let year_list=[];
    for(let index=0;index<inventory.length;index++){
        year_list[index]=inventory[index].car_year;
    }
    return year_list;
   }else{
    return [];
   }
}

module.exports = get_car_year_list;